#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <limits>

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "Vect.h"
#include "Ray.h"
#include "Camera.h"
#include "Color.h"
#include "Source.h"
#include "Light.h"
#include "Object.h"
#include "Sphere.h"
#include "Plane.h"

using namespace std;

struct RGB
{
	double r, g, b;
};

//tutorial: https://www.youtube.com/watch?v=KjHKwCZyAhQ

//creates an .bmp image of the scene
void savebmp (const char *filename, int w, int h, int dpi, RGB *data)
{
	FILE *f;
	int k = w*h;
	int s = 4*k;
	int filesize = 54 + s;
	
	double factor = 39.375;
	int m = static_cast<int>(factor);
	
	int ppm = dpi*m;
	
	unsigned char bmpfileheader[14] = {'B', 'M', 0, 0, 0, 0, 0, 0, 0, 0, 54, 0, 0, 0};
	unsigned char bmpinfoheader[40] = {40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 24, 0};
	
	bmpfileheader[2] = (unsigned char)(filesize);
	bmpfileheader[3] = (unsigned char)(filesize>>8);
	bmpfileheader[4] = (unsigned char)(filesize>>16);
	bmpfileheader[5] = (unsigned char)(filesize>>24);
	
	bmpinfoheader[4] = (unsigned char)(w);
	bmpinfoheader[5] = (unsigned char)(w>>8);
	bmpinfoheader[6] = (unsigned char)(w>>16);
	bmpinfoheader[7] = (unsigned char)(w>>24);
	
	bmpinfoheader[8] = (unsigned char)(h);
	bmpinfoheader[9] = (unsigned char)(h>>8);
	bmpinfoheader[10] = (unsigned char)(h>>16);
	bmpinfoheader[11] = (unsigned char)(h>>24);
	
	bmpinfoheader[21] = (unsigned char)(s);
	bmpinfoheader[22] = (unsigned char)(s>>8);
	bmpinfoheader[23] = (unsigned char)(s>>16);
	bmpinfoheader[24] = (unsigned char)(s>>24);
	
	bmpinfoheader[25] = (unsigned char)(ppm);
	bmpinfoheader[26] = (unsigned char)(ppm>>8);
	bmpinfoheader[27] = (unsigned char)(ppm>>16);
	bmpinfoheader[28] = (unsigned char)(ppm>>24);
	
	bmpinfoheader[29] = (unsigned char)(ppm);
	bmpinfoheader[30] = (unsigned char)(ppm>>8);
	bmpinfoheader[31] = (unsigned char)(ppm>>16);
	bmpinfoheader[32] = (unsigned char)(ppm>>24);
	
	f = fopen(filename, "wb");
	
	fwrite(bmpfileheader,1,14,f);
	fwrite(bmpinfoheader,1,40,f);
	
	for(int i = 0; i < k; i++)
	{
		RGB rgb = data[i];
		
		double red = (data[i].r*255);
		double green = (data[i].g*255);
		double blue = (data[i].b*255);
		
		unsigned char color[3] = {(int)floor(red), (int)floor(green), (int)floor(blue)};
		
		fwrite(color, 1, 3, f);
	}
	
	fclose(f);
}

int closestObjectIndex(vector<double> object_intersections) //return the index of the first object a ray hits
{
	//remember: intersection is a scalar describing distance from camera to intersection point
	int minimum_value;
	
	//prevent unnessesary calculations
	if (object_intersections.size() == 0) //if there are no intersections
	{
		return -1;
	}
	else if (object_intersections.size() == 1) //if there was only one intersecton
	{
		if (object_intersections.at(0) > 0) //if that intersection is greater than 0 then its the index of minimum value
		{
			return 0;
		}
		else //otherwise the only intersection value is negative, the ray missed everything.
		{
			return -1;
		}
	}
	else //otherwise there is more than one intersection, must find the index of the closest object
	{
		double max = 0;
		for (int i = 0; i < object_intersections.size(); i++) //finding maximum intersection value
		{
			if (max < object_intersections.at(i))
			{
				max = object_intersections.at(i);
			}
		}
		
		//finding the minimum positive value from the max value
		if (max > 0) //only looking for positive intersections, "-1" means it missed. 
		{
			for (int j = 0; j < object_intersections.size(); j++)
			{
				if (object_intersections.at(j) > 0 && object_intersections.at(j) <= max)
				{
					max = object_intersections.at(j);
					minimum_value = j;
				}
			}
			
			return minimum_value;
		}
		else // all intersections were negative, ray missed. 
		{
			return -1;
		}
	}
}

Color getColorAt(Vect intersection_position, Vect intersecting_ray_direction, vector<Object*> scene_objects, int index_of_closest_object, vector<Source*> light_sources, double accuracy, double ambientLight)
{
	
	Color closest_object_color = scene_objects.at(index_of_closest_object)->getColor();
	Vect closest_object_normal = scene_objects.at(index_of_closest_object)->getNormal(intersection_position);
	Color final_color = closest_object_color.colorScalar(ambientLight); //scale final color by ambient light factor
	
	for (int light_index = 0; light_index < light_sources.size(); light_index++) //looping through lightsources
	{
		Vect light_direction = light_sources.at(light_index)->getLightPosition().addVect(intersection_position.negative()).normalize();
		
		float cosine_angle = closest_object_normal.dotProduct(light_direction); //angle between closest object intersection normal and light direction
		
		if (cosine_angle > 0) //test for shadows
		{
			bool shadowed = false; //not shadowed by default
			
			Vect distance_to_light = light_sources.at(light_index)->getLightPosition().addVect(intersection_position.negative()).normalize();
			float distance_to_light_magnitude = distance_to_light.magnitude(); //distance to lightsource from intersection point
			
			//creating secondary shadow ray towards the lightsources
			Ray shadow_ray(intersection_position, light_sources.at(light_index)->getLightPosition().addVect(intersection_position.negative()).normalize()); 
		
			vector<double> secondary_intersections;
			
			//find shadow ray intersections
			for (int object_index = 0; object_index < scene_objects.size() && shadowed == false; object_index++) 
			{
				secondary_intersections.push_back(scene_objects.at(object_index)->findIntersection(shadow_ray));
			}

			for (int i = 0; i < secondary_intersections.size(); i++)
			{
				
				if (secondary_intersections.at(i) > accuracy)
				{
					//if the shadow ray intersects with anything before hitting the lightsource the original intersection is shadowed
					if (secondary_intersections.at(i) <= distance_to_light_magnitude) 
					{						
						shadowed = true;
					}
					break;
				}
				
			}
			
			if (shadowed == false)
			{
				//final color is affected by the objects color as well as light color
				final_color = final_color.addColor(closest_object_color.multiplyColor(light_sources.at(light_index)->getColor()).colorScalar(cosine_angle));
			}
		}	
	}
	
	return final_color.clip();
}

int main (int argc, char *argv[])
{
    cout <<"tracing rays" << endl;
    
    int dpi = 72;; //dots per inch
	int width = 720;
	int height = 480;
    
	double aspectratio = (double)width/(double)height;
    double x_amount, y_amount;
    
    double ambientLight = 0.2;
    double accuracy = 0.000001; //to make sure the color value from an intersection is not inside the object
    
    int pixel_index; 
    int n = width*height;
    RGB *pixels = new RGB[n];
    
    //declare xyz and origin vectors
    Vect O(0, 0, 0); //origin
    Vect X(1, 0, 0);
    Vect Y(0, 1, 0);
    Vect Z(0, 0, 1);
    
    //define direction of camera
    Vect camPosition(3.0, 0, -4.0);
    Vect look_at(0.0, 0.0, 0.0); //point camera looks at
    Vect difference(camPosition.getVectX() - look_at.getVectX(), camPosition.getVectY() - look_at.getVectY(), camPosition.getVectZ() - look_at.getVectZ());
    Vect camDirection = difference.negative().normalize();
    Vect camRight = Y.crossProduct(camDirection).normalize();
    Vect camDown = camRight.crossProduct(camDirection);
    Camera scene_camera(camPosition, camDirection, camRight, camDown);
    
    //define colors in range from 0-1: color(r,g,b,s) 
    //some have wrong name, fix later

    Color white(1.0, 1.0, 1.0, 0);
    Color light_green(0.5, 1.0 ,0.5, 0);
    Color purple(0.5, 0.25, 0.25, 0);
    Color gray(0.5, 0.5, 0.5, 0);
    Color black(0.0, 0.0, 0.0, 0);
    Color light_blue(0.5, 0.5, 1.0, 0);
    
    //add lightsources here
    Vect light_position(-7, 10, -10);
    Vect light_position2(7, 10, 10);

	Light scene_light(light_position, white);
	Light scene_light2(light_position2, light_blue);
	
	vector<Source*> light_sources; //a vector of lightsources
	light_sources.push_back(dynamic_cast<Source*>(&scene_light));
	light_sources.push_back(dynamic_cast<Source*>(&scene_light2));
	
	//defining scene objects
	//add spheres here

	Sphere scene_sphere(O, 1, light_blue); //sphere at origin with radius 1
	
	Vect sphere2_pos(0, 0.5, 3);
	Sphere scene_sphere2(sphere2_pos, 1.5, white); 
	
	//add planes here
	Plane scene_plane(Y, -1, purple); //flat plane under the sphere
	
	//add objects to vector
	vector<Object*> scene_objects; //creating a vector of scene objects
	scene_objects.push_back(dynamic_cast<Object*>(&scene_sphere));
	scene_objects.push_back(dynamic_cast<Object*>(&scene_sphere2));
    scene_objects.push_back(dynamic_cast<Object*>(&scene_plane));
    
    //loop through every pixel in the image
    for (int x = 0; x < width; x++)
    {
	 	for (int y = 0; y < height; y++)
	 	{
			pixel_index = y*width + x;
		 	
		 	//without anti-aliasing.
		 	//offset the position from where the camera is pointing in order to 
			//create rays that goes to the left, right, up and down from the direction
			//the camera is pointed. This is in order to make the rays
			//pass through the entire image plane.
		 	if (width > height) //the image is wider than it is tall
		 	{
				x_amount = ((x + 0.5)/width)*aspectratio - (((width-height)/(double)height)/2);
				y_amount = ((height - y) + 0.5)/height;
			}
		 	else if (height > width) //the image is taller than it is wide
			{
				x_amount = (x + 0.5)/width;
				y_amount = (((height - y) + 0.5)/height)/aspectratio - (((height - width)/(double)width)/2);	
			}
			else //the image is square
			{
				x_amount = (x + 0.5)/width; 
				y_amount = ((height - y) + 0.5)/height;
			}
			
			//creating rays
			Vect cam_ray_origin = scene_camera.getCameraPosition();
			//using ofset value to get correct ray direction
			Vect cam_ray_direction = camDirection.addVect(camRight.multiplyVect(x_amount - 0.5).addVect(camDown.multiplyVect(y_amount - 0.5))).normalize();
			
			Ray cam_ray (cam_ray_origin, cam_ray_direction);
			
			//array of intersections
			vector<double> intersections;
			
			for (int index = 0; index < scene_objects.size(); index++) //looping through the objects in the scene
			{
				//finds intersections in object pointed to by scene objects vector and pushes it into intersections vector
				intersections.push_back(scene_objects.at(index)->findIntersection(cam_ray)); 
			}
			
			int index_of_closest_object = closestObjectIndex(intersections); //finds index of intersection closest to the camera
			
		 	//return color
		 	if(index_of_closest_object == -1) //ray did not hit any objects, set background black
			{ 
		 		pixels[pixel_index].r = 0;
		 		pixels[pixel_index].g = 0;
		 		pixels[pixel_index].b = 0;
			}
			else //index corresponds to an object in the scene
			{
				/*Color pixel_color = scene_objects.at(index_of_closest_object)->getColor();
				
				pixels[pixel_index].r = pixel_color.getColorRed();
		 		pixels[pixel_index].g = pixel_color.getColorGreen();
		 		pixels[pixel_index].b = pixel_color.getColorBlue();*/
				
				if (intersections.at(index_of_closest_object) > accuracy) 
				{
					//find the position and direction vectors at the point of intersection
					
					Vect intersection_position = cam_ray_origin.addVect(cam_ray_direction.multiplyVect(intersections.at(index_of_closest_object)));
					Vect intersecting_ray_direction = cam_ray_direction;
					
					//getting the color at the intersection
					Color intersection_color = getColorAt(intersection_position, intersecting_ray_direction, scene_objects, index_of_closest_object, light_sources, accuracy, ambientLight); 
					
					pixels[pixel_index].r = intersection_color.getColorRed();
		 			pixels[pixel_index].g = intersection_color.getColorGreen();
		 			pixels[pixel_index].b = intersection_color.getColorBlue();
				}
			}
		}
	}
	
	savebmp("scene.bmp", width, height, dpi, pixels);
	
    return 0;
}



