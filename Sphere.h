#ifndef _SPHERE_H
#define _SPHERE_H

#include "math.h"
#include "Vect.h"
#include "Object.h"
#include "Color.h"

class Sphere : public Object
{
	Vect center;
	double radius;
	Color color;
	
	public:
		
	Sphere();
	
	Sphere(Vect, double, Color);
	
	Vect getSphereCenter() { return center; }
	double getSphereRadius() { return radius; }
	virtual Color getColor() { return color; }
	
	virtual Vect getNormal(Vect point) //finding normal on point of intersection
	{
		//normal points away from center of sphere
		Vect normalVect = point.addVect(center.negative()).normalize();
		return normalVect;
	}
	
	virtual double findIntersection(Ray ray) // scalar distance from ray origin to intersection point
	{
		//find xyz of ray origin
		Vect ray_origin = ray.getRayOrigin();
		double ray_origin_x = ray_origin.getVectX();
		double ray_origin_y = ray_origin.getVectY();
		double ray_origin_z = ray_origin.getVectZ();
		
		//find xyz ray direction
		Vect ray_direction = ray.getRayDirection();
		double ray_direction_x = ray_direction.getVectX();
		double ray_direction_y = ray_direction.getVectY();
		double ray_direction_z = ray_direction.getVectZ();
		
		//find xyz of center of sphere
		Vect sphere_center = center;
		double sphere_center_x = sphere_center.getVectX();
		double sphere_center_y = sphere_center.getVectY();
		double sphere_center_z = sphere_center.getVectZ();
		
		double a = 1; //normalized
		double b = (2*(ray_origin_x - sphere_center_x)*ray_direction_x) + (2*(ray_origin_y - sphere_center_y)*ray_direction_y) + (2*(ray_origin_z - sphere_center_z)*ray_direction_z);
		double c = pow(ray_origin_x - sphere_center_x, 2) + pow(ray_origin_y - sphere_center_y, 2) + pow(ray_origin_z - sphere_center_z, 2) - (radius*radius);
		
		double discriminant = b*b - 4*c;
		
		if (discriminant > 0) //ray intersects sphere
		{
			//first root
			double root_1 = ((-1*b - sqrt(discriminant))/2) - 0.000001;
			
			if (root_1 > 0) //first root is smallest positive root
			{
				return root_1;
			}
			else //second root is smallest positive root
			{
				double root_2 = ((sqrt(discriminant) - b)/2) - 0.000001;
				return root_2;
			}
		}
		else //ray missed the sphere
		{
			return -1;
		}
	}
};

Sphere::Sphere()
{
	center = Vect(0, 0, 0);
	radius = 1.0; 
	color = Color(1.0, 1.0, 1.0, 0);
}

Sphere::Sphere(Vect ce, double r, Color co)
{
	center = ce;
	radius = r;
	color = co;
}

#endif
