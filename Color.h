#ifndef _COLOR_H
#define _COLOR_H

#include "math.h"

class Color 
{
	double red, green, blue, special;
	
	public:
		
	Color();
	
	Color(double, double, double, double);
	
	double getColorRed() {return red; }
	double getColorGreen() {return green; }
	double getColorBlue() {return blue; }
	//special = set up for use with reflection and shinyness and potential other fancy effects
	double getColorSpecial() {return special; } 
	
	double setColorRed(double redValue) {red = redValue; }
	double setColorGreen(double greenValue) {green = greenValue; }
	double setColorBlue(double blueValue) {blue = blueValue; }
	double setColorSpecial(double specialValue) {special = specialValue; }
	
	double brightness()
	{
		return (red + green + blue)/3;
	}
	
	Color colorScalar(double scalar)
	{
		return Color(red*scalar, green*scalar, blue*scalar, special);
	}
	
	Color addColor(Color color)
	{
		return Color(red + color.getColorRed(), green + color.getColorGreen(), blue + color.getColorBlue(), special);
	}
	
	Color multiplyColor(Color color)
	{
		return Color(red*color.getColorRed(), green*color.getColorGreen(), blue*color.getColorBlue(), special);
	}
	
	Color averageColor(Color color)
	{
		return Color((red + color.getColorRed())/2, (green + color.getColorGreen())/2, (blue + color.getColorBlue())/2, special);
	}
	
	Color clip() //stops clipping of colors, risk wierd color effects without
	{
		double all_light = red + green + blue;
		double excess_light = all_light - 3;
		if (excess_light > 0)
		{
			red = red + excess_light*(red/all_light);
			green = green + excess_light*(green/all_light);
			blue = blue + excess_light*(blue/all_light);
		}
		if (red > 1) {red = 1;}
		if (green > 1) {green = 1;}
		if (blue > 1) {blue = 1;}
		if (red < 0) {red = 0;}
		if (green < 0) {green = 0;}
		if (blue < 0) {blue = 0;}
		
		return Color (red, green, blue, special);
	}
};

Color::Color()
{
	red = 0.5;
	green = 0.5;
	blue = 0.5;
}

Color::Color(double r, double g, double b, double s)
{
	red = r;
	green = g;
	blue = b;
	special = s;
}

#endif
