#ifndef _Plane_H
#define _Plane_H

#include "Vect.h"
#include "Object.h"
#include "Color.h"

class Plane : public Object
{
	Vect normal;
	double distance; //from origin
	Color color;
	
	public:
		
	Plane();
	
	Plane(Vect, double, Color);
	
	Vect getPlaneNormal() { return normal; }
	double getPlaneDistance() { return distance; }
	Color getColor() { return color; }
	
	Vect getNormal(Vect point)
	{
		return normal;
	}
	
	double findIntersection(Ray ray) // scalar distance from ray origin to intersection point
	{
		Vect ray_direction = ray.getRayDirection();
		double a = ray_direction.dotProduct(normal);
		
		if (a == 0) //ray is paralell to plane
		{
			return -1;
		}
		else
		{
			double b = normal.dotProduct(ray.getRayOrigin().addVect(normal.multiplyVect(distance).negative()));
			return -1*b/a; //distance from ray origin to intersection point
		}
	}
};

Plane::Plane()
{
	normal = Vect(1, 0, 0);
	distance = 0; 
	color = Color(0.5, 0.5, 0.5, 0);
}

Plane::Plane(Vect n, double d, Color c)
{
	normal = n;
	distance = d;
	color = c;
}

#endif
