#ifndef _OBJECT_H
#define _OBJECT_H

#include "Ray.h"
#include "Vect.h"
#include "Color.h"

class Object 
{
	public:
		
	Object();

	virtual Color getColor() { return Color(0, 0, 0, 0); } //virtual to make sure the correct functions from the derived object classes are called in main
	
	virtual Vect getNormal(Vect point)
	{
		return Vect(0, 0, 0);
	}
	
	virtual double findIntersection(Ray ray) 
	{
		return 0;
	}
};

Object::Object() {}

#endif
