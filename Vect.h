#ifndef _VECT_H
#define _VECT_H

#include "math.h"

class Vect 
{
	double x, y, z;
	
	public:
		
	Vect();
	
	Vect(double, double, double);
	
	double getVectX() {return x; }
	double getVectY() {return y; }
	double getVectZ() {return z; }
	
	
	//linear algebra functions
	double magnitude()
	{
		return sqrt((x*x) + (y*y) + (z*z)); //scalar
	}
	
	Vect normalize() 
	{
		double magnitude = sqrt((x*x) + (y*y) + (z*z));
		return Vect(x/magnitude, y/magnitude, z/magnitude);
	}
	
	Vect negative()
	{
		return Vect(-x, -y, -z);
	}
	
	double dotProduct(Vect v) // V1 dot V2 = V1x*V2x + V1y*V2y + V1z*V2z
	{
		return x*v.getVectX() + y*v.getVectY() + z*v.getVectZ(); //scalar
	}
	
	Vect crossProduct(Vect v) //V1 X V2 = (V1y*V2z-V1z*V2y, V1z*V2x-V1x*V2z, V1x*V2y-V1y*V2x) 
	{
		return Vect(y*v.getVectZ() - z*v.getVectY(), z*v.getVectX() - x*getVectZ(), x*v.getVectY() - y*v.getVectX());	
	}
	
	Vect addVect(Vect v) 
	{
		return Vect(x + v.getVectX(), y + v.getVectY(), z + v.getVectZ());	
	}
	
	Vect multiplyVect(double scalar) //scalar multiplication
	{
		return Vect(x*scalar, y*scalar, z*scalar);
	}
};

Vect::Vect()
{
	x = 0;
	y = 0;
	z = 0;
}

Vect::Vect(double i, double j, double k)
{
	x = i;
	y = j;
	z = k;
}

#endif
