#ifndef _CAMERA_H
#define _CAMERA_H

#include "math.h"

class Camera 
{
	Vect camPosition, camDirection, camRight, camDown;
	
	public:
		
	Camera();
	
	Camera(Vect, Vect, Vect, Vect);
	
	Vect getCameraPosition() { return camPosition; }
	Vect getCameraDirection() { return camDirection; } //front of camera, direction it is facing
	Vect getCameraRight() { return camRight; } //the right side of the camera, helps define orientation
	Vect getCameraDown() { return camDown; } //the bottom of the camera, helps define orientation
	
};

Camera::Camera()
{
	camPosition = Vect(0,0,0);
	camDirection = Vect(0,0,1);
	camRight = Vect(0,0,0);
	camDown = Vect(0,0,1);
}

Camera::Camera(Vect pos, Vect dir, Vect right, Vect down)
{
	camPosition = pos;
	camDirection = dir;
	camRight = right;
	camDown = down;
}

#endif
